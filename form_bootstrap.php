<?php
function verifica_campo($texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  return $texto;
}

$nome = $email = $tipo = "";
$erro = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if(empty($_POST["nome"])){
    $erro_nome = "Nome é obrigatório.";
    $erro = true;
  }
  else{
    $nome = verifica_campo($_POST["nome"]);
  }

  if(empty($_POST["email"])){
    $erro_email = "Email é obrigatório.";
    $erro = true;
  }
  else{
    $email = verifica_campo($_POST["email"]);
  }

  if(empty($_POST["tipo"])){
    $erro_tipo = "Tipo é obrigatório.";
    $erro = true;
  }
  else{
    $tipo = verifica_campo($_POST["tipo"]);
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Teste PHP</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h1 class="page-header">Teste Formulário PHP</h1>

      <?php if ($_SERVER["REQUEST_METHOD"] == "POST"): ?>
        <?php if (!$erro): ?>
          <div class="alert alert-success">
            Dados recebidos com sucesso:
            <ul>
              <li><strong>Nome</strong>: <?php echo $nome ?>;</li>
              <li><strong>Email</strong>: <?php echo $email ?>;</li>
              <li><strong>Tipo</strong>: <?php echo $tipo ?>;</li>
              <?php // limpa o formulário.
                $nome = $email = $tipo = "";
              ?>
            </ul>
          </div>
        <?php else: ?>
          <div class="alert alert-danger">
            Erros no formulário.
          </div>
        <?php endif; ?>
      <?php endif; ?>

      <form class="form-horizontal" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">

        <div class="form-group <?php if(!empty($erro_nome)){echo "has-error";}?>">
          <label for="inputNome" class="col-sm-2 control-label">Nome</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nome" placeholder="Nome" value="<?php echo $nome; ?>">
            <?php if (!empty($erro_nome)){ ?>
              <span class="help-block"><?php echo $erro_nome ?></span>
            <?php } ?>
          </div>
        </div>

        <div class="form-group <?php if(!empty($erro_email)){echo "has-error";}?>">
          <label for="inputEmail" class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $email; ?>">
            <?php if (!empty($erro_email)): ?>
              <span class="help-block"><?php echo $erro_email ?></span>
            <?php endif; ?>
          </div>
        </div>

        <div class="form-group <?php if(!empty($erro_tipo)){echo "has-error";}?>">
          <div class="col-sm-offset-2 col-sm-10">
            <label class="radio-inline">
              <input type="radio" name="tipo" id="radioAluno" value="aluno" <?php echo ($tipo == "aluno" ? "checked" : "") ?>> Aluno
            </label>

            <label class="radio-inline">
              <input type="radio" name="tipo" id="radioProfesssor" value="professor" <?php echo ($tipo == "professor" ? "checked" : "") ?>> Professor
            </label>
            <?php if (!empty($erro_tipo)){ ?>
              <span class="help-block"><?php echo $erro_tipo ?></span>
            <?php } ?>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Enviar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>